defmodule MatrixAppServiceWeb.Router do
  use MatrixAppServiceWeb, :router

  require MatrixAppService.Phoenix.Router

  MatrixAppService.Phoenix.Router.routes()
end
