defmodule MatrixAppServiceWeb.V1.ThirdPartyController do
  use MatrixAppServiceWeb, :controller

  def show(conn, _params) do
    send_resp(conn, 200, "")
  end
end
