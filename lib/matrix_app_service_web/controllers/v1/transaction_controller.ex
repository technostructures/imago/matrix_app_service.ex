defmodule MatrixAppServiceWeb.V1.TransactionController do
  use MatrixAppServiceWeb, :controller

  # %{"access_token" => access_token,
  #   "events" => [
  #     %{"age" => 199,
  #       "content" => %{
  #         "body" => "test",
  #         "msgtype" => "m.room.message"},
  #       "event_id" => "$pMDN7TcsCapjLRYoITwryk0anpQGs60y2Eng3lxropc",
  #       "origin_server_ts" => 1588950428966,
  #       "room_id" => "!SrVtqmyqzuaMnfVImP:matrix.imago.local",
  #       "sender" => "@alice:matrix.imago.local",
  #       "type" => "m.room.message",
  #       "unsigned" => %{"age" => 199},
  #       "user_id" => "@alice:matrix.imago.local"}],
  #   "txn_id" => "269"}

  defp create_event(%{
         "age" => age,
         "content" => content,
         "event_id" => event_id,
         "origin_server_ts" => origin_server_ts,
         "room_id" => room_id,
         "sender" => sender,
         "state_key" => state_key,
         "type" => type,
         "unsigned" => unsigned,
         "user_id" => user_id
       }) do
    module = Application.fetch_env!(:matrix_app_service, :transaction_module)

    event = %MatrixAppService.Event{
      age: age,
      content: content,
      event_id: event_id,
      origin_server_ts: origin_server_ts,
      room_id: room_id,
      sender: sender,
      state_key: state_key,
      type: type,
      unsigned: unsigned,
      user_id: user_id
    }

    module.new_event(event)
  end

  defp create_event(%{
         "age" => age,
         "content" => content,
         "event_id" => event_id,
         "origin_server_ts" => origin_server_ts,
         "room_id" => room_id,
         "sender" => sender,
         "type" => type,
         "unsigned" => unsigned,
         "user_id" => user_id
       }) do
    module = Application.fetch_env!(:matrix_app_service, :transaction_module)

    event = %MatrixAppService.Event{
      age: age,
      content: content,
      event_id: event_id,
      origin_server_ts: origin_server_ts,
      room_id: room_id,
      sender: sender,
      state_key: nil,
      type: type,
      unsigned: unsigned,
      user_id: user_id
    }

    module.new_event(event)
  end

  def create(conn, %{"events" => events}) do
    Enum.each(events, &create_event(&1))
    send_resp(conn, 200, "{}")
  end
end
