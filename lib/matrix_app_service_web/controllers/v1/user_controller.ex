defmodule MatrixAppServiceWeb.V1.UserController do
  use MatrixAppServiceWeb, :controller

  def show(conn, _params) do
    send_resp(conn, 404, "")
  end
end
