defmodule MatrixAppServiceWeb.ErrorView do
  use MatrixAppServiceWeb, :view

  def render("401.json", _assigns) do
    %{errcode: "EX.MAP.UNAUTHORIZED"}
  end

  def render("403.json", _assigns) do
    %{errcode: "EX.MAP.FORBIDDEN"}
  end

  # By default, Phoenix returns the status message from
  # the template name. For example, "404.json" becomes
  # "Not Found".
  def template_not_found(template, _assigns) do
    %{errors: %{detail: Phoenix.Controller.status_message_from_template(template)}}
  end
end
