defmodule MatrixAppService.Phoenix.Router do
  defmacro routes() do
    quote do
      pipeline :matrix_api do
        plug :accepts, ["json"]
        plug MatrixAppService.AuthPlug
      end

      path = Application.compile_env(:matrix_app_service, :path, "/")
      scope path, MatrixAppServiceWeb.V1 do
        pipe_through :matrix_api

        put "/transactions/:txn_id", TransactionController, :create

        get "/users/:user_id", UserController, :show
        get "/rooms/:room_alias", RoomController, :show

        get "/thirdparty/protocol/:protocol", ThirdPartyController, :show
        get "/thirdparty/user/:protocol", ThirdPartyController, :show
        get "/thirdparty/location/:protocol", ThirdPartyController, :show
        get "/thirdparty/location", ThirdPartyController, :show
        get "/thirdparty/user", ThirdPartyController, :show
      end
    end
  end
end
