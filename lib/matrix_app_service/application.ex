defmodule MatrixAppService.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      # MatrixAppServiceWeb.Telemetry,
      # Start the PubSub system
      # {Phoenix.PubSub, name: MatrixAppService.PubSub},
      # Start the Endpoint (http/https)
      # MatrixAppServiceWeb.Endpoint
      # Start a worker by calling: MatrixAppService.Worker.start_link(arg)
      # {MatrixAppService.Worker, arg}
    ]
    children = if Application.get_env(:matrix_app_service, :standalone, false) do
      [MatrixAppServiceWeb.Endpoint | children]
    else
      children
    end

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MatrixAppService.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    if Application.get_env(:matrix_app_service, :standalone, false) do
      MatrixAppServiceWeb.Endpoint.config_change(changed, removed)
    end

    :ok
  end
end
