defmodule MatrixAppService.AuthPlug do
  @behaviour Plug
  import Plug.Conn
  require Logger

  @impl Plug
  def init(opts) do
    opts
  end

  @impl Plug
  def call(%Plug.Conn{params: %{"access_token" => hs_token}} = conn, _) do
    config_hs_token = Application.fetch_env!(:matrix_app_service, :homeserver_token)
    with ^config_hs_token <- hs_token do
      conn
    else
      _ ->
        Logger.warn("Received invalid homeserver token")
        respond_error(conn, 403)
    end
  end

  def call(conn, _config_hs_token) do
    Logger.warn("No homeserver token provided")
    respond_error(conn, 401)
  end

  defp respond_error(conn, error_code) do
    conn
    |> put_status(error_code)
    |> Phoenix.Controller.put_view(MatrixAppServiceWeb.ErrorView)
    |> Phoenix.Controller.render("#{error_code}.json")
    |> halt
  end
end
