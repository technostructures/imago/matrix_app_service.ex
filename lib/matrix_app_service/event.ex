defmodule MatrixAppService.Event do
  @type t :: %__MODULE__{
          age: integer(),
          content: map(),
          event_id: String.t(),
          origin_server_ts: integer(),
          room_id: String.t(),
          sender: String.t(),
          state_key: String.t(),
          type: String.t(),
          unsigned: map(),
          user_id: String.t()
        }

  defstruct age: nil,
            content: %{},
            event_id: nil,
            origin_server_ts: nil,
            room_id: nil,
            sender: nil,
            state_key: "",
            type: nil,
            unsigned: %{},
            user_id: nil
end
