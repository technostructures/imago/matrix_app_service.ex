defmodule MatrixAppService.TransactionModule do
  @callback new_event(MatrixAppService.Event.t()) :: any()
end
